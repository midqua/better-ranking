// ==UserScript==
// @name         Better Ranking
// @namespace    http://tampermonkey.net/
// @version      0.2.4
// @description  try to take over the world!
// @author       You
// @updateURL    https://gitlab.com/midqua/better-ranking/-/raw/main/src/better-banking-tm.js
// @downloadURL  https://gitlab.com/midqua/better-ranking/-/raw/main/src/better-banking-tm.js
// @match        https://magiceden.io
// @match        https://magiceden.io/*
// @match        https://alpha.art
// @match        https://alpha.art/*
// @match        https://opensea.io
// @match        https://opensea.io/*
// @icon         https://www.google.com/s2/favicons?domain=magiceden.io
// @require      https://cdnjs.cloudflare.com/ajax/libs/axios/0.24.0/axios.min.js
// @grant        none
// ==/UserScript==

(function () {
  "use strict";
  const ITEM_EXPIRE = 86400 * 1000; // 1 day, expire only used in error case ( try find new data )
  const ClearCacheButtonText = "Loaded";
  const ClearCacheButtonLoading = "Loading...";
  const ClearCacheButtonError = "Error";
  const ClearCacheButtonNA = "";

  let clearCacheButton = document.createElement("BUTTON");
  clearCacheButton.innerText = ClearCacheButtonText;
  clearCacheButton.style.cssText =
    "position:fixed;top:24px;left:180px;z-index:9999999";
  document.body.appendChild(clearCacheButton);

  let itemKey = ``;
  clearCacheButton.addEventListener("click", () => {
    clearlocalStorage(itemKey);
  });

  const disabledClearCacheButton = (disabled) => {
    if (disabled) {
      clearCacheButton.innerText = ClearCacheButtonLoading;
      clearCacheButton.disabled = true;
    } else {
      clearCacheButton.innerText = ClearCacheButtonText;
      //clearCacheButton.disabled = false;// force one time use for production
    }
  };

  const errorClearCacheButton = () => {
    clearCacheButton.innerText = ClearCacheButtonError;
    //clearCacheButton.disabled = false;// force one time use for production
  };

  // ------------ common functions ----------
  const clearlocalStorage = (itemKey) => {
    localStorage.removeItem(itemKey);
    console.log("Cleared cache:", itemKey);
  };

  const addRankListToLocalStorage = async (itemKey, rankList) => {
    let item = { lastUpdated: new Date().getTime(), rankList };
    localStorage.setItem(itemKey, JSON.stringify(item));
    return item;
  };

  const addRankToTextElement = (textEle, list, color) => {
    if (textEle) {
      let name = textEle.textContent;
      textEle.style.whiteSpace = "normal";

      for (let i = 0; i < list.length; i++) {
        let listName = list[i].n.trim().replace(/#0+([0-9]+$)/, `#$1`); // trim leading zero in id in name
        name = name.trim().replace(/#0+([0-9]+$)/, `#$1`); // trim leading zero in id in name
        if (listName === name) {
          //added rank element would not be matched because inner text has been added a child

          let node = document.createElement("SPAN"); // Append the text to <li>
          node.innerText += " <" + list[i].r + ">";
          node.style.color = color;
          node.style.fontSize = "smaller";
          node.style.fontWeight = 500;

          if (textEle.childElementCount > 0) textEle.removeChild();
          textEle.appendChild(node);
          break;
        }
      }
    }
  };

  // ------------ fetch from ranking apis ----------
  // For magiceden, alphaart general
  const fetchFromMoonrank = async ({ collections, itemKey }) => {
    console.log("fetching....");
    disabledClearCacheButton(true);
    let rankList = [];
    let error = {};
    for (let i = 0; i < collections.length; i++) {
      //loop for all names for trial and error
      try {
        let { data } = await axios.get(
          `https://crypto-web-prod.herokuapp.com/myProxy?url=https%3A%2F%2Fmoonrank.app%2Fmints%2F${
            collections[i]
          }%3Fafter%3D0%26seen%3D5888%26complete%3Dtrue${
            itemKey ? `&key=${itemKey}` : ``
          }`,
          { timeout: 120000 }
        );
        rankList = data.mints.map((mint) => ({ n: mint.name, r: mint.rank }));
      } catch (err) {
        console.error({ error: err.message });
        error = { error: err.message };
      }
      if (rankList.length > 0) {
        disabledClearCacheButton(false);
        return rankList;
      }
    }
    errorClearCacheButton();
    return error;
  };

  const fetchFromLuckyKittensRank = async ({ itemKey }) => {
    console.log("fetching....");
    disabledClearCacheButton(true);
    let rankList = [];
    let error = {};
    try {
      let { data } = await axios.get(
        `https://crypto-web-prod.herokuapp.com/myProxy?url=https%3A%2F%2Fluckykittens.io%2Franking.json${
          itemKey ? `&key=${itemKey}` : ``
        }`,
        { timeout: 120000 }
      );
      rankList = data.map((mint) => ({
        n: `Lucky Kitten #${mint.number}`,
        r: mint.ranking,
      }));
    } catch (err) {
      console.error({ error: err.message });
      error = { error: err.message };
    }
    if (rankList.length > 0) {
      disabledClearCacheButton(false);
      return rankList;
    }
    errorClearCacheButton();
    return error;
  };

  const fetchFromNovalaunchRank = async ({ site }) => {
    console.log("fetching....Nova launch site");
    disabledClearCacheButton(true);
    let rankList = [];
    let error = {};
    try {
      let { data } = await axios.get(
        `https://crypto-web-prod.herokuapp.com/myProxy?url=https%3A%2F%2F${site}%2Frarity`,
        { timeout: 5000 }
      );
      let parser = new DOMParser();
      let htmlDoc = parser.parseFromString(data, "text/html");
      rankList = Array.from(
        htmlDoc.querySelectorAll("#all-mints > .nft-container")
      ).map((ele) => {
        return {
          n: ele.querySelector("h6 > a").textContent,
          r: parseInt(ele.querySelector("p").textContent.match(/#(.*?)$/)[1]),
        };
      });
    } catch (error) {
      console.error({ error: error.message });
      error = { error: error.message };
    }
    if (rankList.length > 0) {
      disabledClearCacheButton(false);
      return rankList;
    }
    errorClearCacheButton();
    return error;
  };

  const fetchFromNiftyRiver = async ({ collections, itemKey }) => {
    let [assetContractAddress] = collections;
    console.log("fetching....");
    disabledClearCacheButton(true);
    let rankList = [];
    let error = {};
    try {
      let { data } = await axios.get(
        `https://crypto-web-prod.herokuapp.com/myProxy?url=https%3A%2F%2Fwww.niftyriver.io%2Fapi%2Fplatforms%2Fcollection%2F${assetContractAddress}%2Frarity%2F%3Fpage_size%3D20000%26page%3D1${
          itemKey ? `&key=${itemKey}` : ``
        }`,
        { timeout: 120000 }
      );
      rankList = data.results.map(({ name, token_id, rarity_metadata }) => {
        // to prevent opensea unmatched the nifty name, always use token id as name
        if (name.match(/#[0-9]+$/)) {
          name = name.replace(/#[0-9]+$/, `#${token_id}`);
        }
        return {
          n: name,
          r: rarity_metadata.index + 1,
        };
      });
    } catch (err) {
      console.error({ error: err.message });
      error = { error: err.message };
    }
    if (rankList.length > 0) {
      disabledClearCacheButton(false);
      return rankList;
    }
    errorClearCacheButton();
    return error;
  };

  // ------------ render for market place pages ----------
  const renderForMagiceden = (list) => {
    Array.from(document.getElementsByClassName("grid-card__main")).forEach(
      (main) => {
        let textEle = main.getElementsByTagName("h6")[0];
        addRankToTextElement(textEle, list, "pink");
      }
    );
  };

  const renderForAlphaart = (list) => {
    Array.from(document.querySelectorAll("a[href^='/t/']")).forEach((main) => {
      let textEleItemPage = main.getElementsByTagName("h3")[0];
      addRankToTextElement(textEleItemPage, list, "purple");
      let textEleActivityPage = main.querySelector("span[class^=ml-1]");
      addRankToTextElement(textEleActivityPage, list, "purple");
    });
  };

  const renderForOpensea = (list) => {
    //items page
    Array.from(
      document.querySelectorAll(
        "div.AssetsSearchView--assets div[role=grid] > div"
      )
    ).forEach((main) => {
      let textEle = main.querySelector("div.AssetCardFooter--name");
      addRankToTextElement(textEle, list, "purple");
    });

    //activity page
    Array.from(
      document.querySelectorAll(
        "div[role=listitem] span[class*=Itemreact__ItemTitle] > a > div"
      )
    ).forEach((textEle) => {
      addRankToTextElement(textEle, list, "purple");
    });
  };
  // ------------ render for profile pages ----------
  const renderForMagicedenProfile = () => {
    Array.from(document.querySelectorAll("div.tw-mt-4 > .tw-w-full")).forEach(
      (section) => {
        let anchor = section.querySelector("div.tw-items-center>a");
        let collection = anchor
          .getAttribute("href")
          .match(/^\/marketplace\/(.*?)$/)[1];

        let itemKey = `tm-addrank-magiceden-${collection}`;
        let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
        let list = (item || {}).rankList || [];

        if (list.length > 0) {
          Array.from(section.getElementsByClassName("card__main")).forEach(
            (main) => {
              let textEle = main.getElementsByTagName("h6")[0];
              addRankToTextElement(textEle, list, "pink");
            }
          );
        }
      }
    );
  };

  const renderForAlphaartProfile = () => {
    Array.from(document.querySelectorAll("main > a")).forEach((anchor) => {
      let collection = anchor
        .getAttribute("href")
        .match(/^\/collection\/(.*?)$/)[1];

      let itemKey = `tm-addrank-alphaart-${collection}`;
      let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
      let list = (item || {}).rankList || [];

      let section = anchor.nextSibling.nextSibling;
      if (list.length > 0) {
        Array.from(section.querySelectorAll("a.group")).forEach((main) => {
          let textEle = main.getElementsByTagName("h3")[0];
          addRankToTextElement(textEle, list, "purple");
        });
      }
    });
  };

  const renderForOpenseaProfile = () => {
    Array.from(
      document.querySelectorAll(
        "div.AssetSearchView--results div[role=grid] > div"
      )
    ).forEach((gridcell) => {
      let collection = gridcell
        .querySelector("a.AssetCardFooter--collection-name")
        .getAttribute("href")
        .match(/^\/collection\/(.*?)$/)[1];

      let itemKey = `tm-addrank-opensea-${collection}`;
      let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
      let list = (item || {}).rankList || [];

      if (list.length > 0) {
        let textEle = gridcell.querySelector("div.AssetCardFooter--name");
        addRankToTextElement(textEle, list, "purple");
      }
    });
  };

  // ------------ render for item detail pages ----------
  const renderForMagicedenItemDetail = () => {
    let anchor = document.querySelector("div.content a");
    let collection = anchor
      .getAttribute("href")
      .match(/^\/marketplace\/(.*?)$/)[1];

    let itemKey = `tm-addrank-magiceden-${collection}`;
    let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
    let list = (item || {}).rankList || [];

    if (list.length > 0) {
      let textEle = document.querySelector("h3.item-title");
      addRankToTextElement(textEle, list, "pink");
    }
  };

  const renderForAlphaartItemDetail = () => {
    let anchor = document.querySelector(
      "#root > div > div.mx-auto > div > div:nth-child(2) div.flex > div:nth-child(2) a"
    );
    let collection = anchor
      .getAttribute("href")
      .match(/^\/collection\/(.*?)$/)[1];

    let itemKey = `tm-addrank-alphaart-${collection}`;
    let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
    let list = (item || {}).rankList || [];

    if (list.length > 0) {
      let textEle = document.querySelector(
        "#root > div > div.mx-auto > div > div:nth-child(2) div.flex h1"
      );
      addRankToTextElement(textEle, list, "purple");
    }
  };

  const renderForOpenseaItemDetail = () => {
    let anchor = document.querySelector(
      "div[class^=CollectionLinkreact__DivContainer] a"
    );
    let collection = anchor
      .getAttribute("href")
      .match(/^\/collection\/(.*?)$/)[1];

    let itemKey = `tm-addrank-opensea-${collection}`;
    let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
    let list = (item || {}).rankList || [];

    if (list.length > 0) {
      let textEle = document.querySelector("h1.item--title");
      addRankToTextElement(textEle, list, "purple");
    }
  };

  //main function
  const addRank = async () => {
    console.log("Start main function...addrank()");
    clearCacheButton.innerText = ClearCacheButtonText;
    if (location.href.match("https://magiceden.io/me")) {
      // magiceden wallet
      renderForMagicedenProfile();
    } else if (location.href.match("https://alpha.art/user/")) {
      // alphaart wallet
      renderForAlphaartProfile();
    } else if (
      location.href.match("https://opensea.io/account") ||
      location.href.match("https://opensea.io/0x")
    ) {
      // opensea wallet
      renderForOpenseaProfile();
    } else if (location.href.match("https://magiceden.io/item-details/")) {
      // magiceden item detail
      renderForMagicedenItemDetail();
    } else if (location.href.match("https://alpha.art/t/")) {
      // alphaart item detail
      renderForAlphaartItemDetail();
    } else if (location.href.match("https://opensea.io/assets/")) {
      // opensea item detail
      renderForOpenseaItemDetail();
    } else {
      itemKey = ``;
      let collection = ``;
      let fetchFunctionData = {};
      let fetchFunction;
      let renderFunction = () => {};
      if (location.href.match("https://opensea.io/collection/")) {
        let assetContractAddress = ``;

        //activity page
        if (location.href.match("tab=activity")) {
          collection =
            location.href.match(/\/collection\/(.*?)\?tab=activity/)[1] || ``;
          itemKey = `tm-addrank-opensea-${collection}`;
          renderFunction = renderForOpensea;
        } else {
          //items page

          let anchor = document.querySelector(
            "article[class^=Assetreact__AssetCard] a"
          );
          if (anchor) {
            let href = anchor.getAttribute("href");
            if (href) {
              assetContractAddress = href.match(/^\/assets\/(.*?)\//)[1];
              console.log({ assetContractAddress });
            }
          }
          if (!assetContractAddress) {
            return; //break function
          }
          collection =
            location.pathname.match(/^\/collection\/(.*?)$/)[1] || ``;
          itemKey = `tm-addrank-opensea-${collection}`;
          fetchFunctionData = { collections: [assetContractAddress], itemKey };
          fetchFunction = fetchFromNiftyRiver;
          renderFunction = renderForOpensea;
        }
      } else if (
        location.href.match("https://magiceden.io/marketplace/lucky_kittens")
      ) {
        collection = `lucky_kittens`;
        itemKey = `tm-addrank-magiceden-${collection}`;
        fetchFunctionData = { itemKey };
        fetchFunction = fetchFromLuckyKittensRank;
        renderFunction = renderForMagiceden;
      } else if (
        location.href.match("https://magiceden.io/marketplace/monkey_kingdom")
      ) {
        collection = `monkey_kingdom`;
        itemKey = `tm-addrank-magiceden-${collection}`;
        fetchFunctionData = { site: `monkeykingdom.io` };
        fetchFunction = fetchFromNovalaunchRank;
        renderFunction = renderForMagiceden;
      } else if (
        location.href.match("https://magiceden.io/marketplace/bull_empire")
      ) {
        collection = `bull_empire`;
        itemKey = `tm-addrank-magiceden-${collection}`;
        fetchFunctionData = { site: `bullempire.io` };
        fetchFunction = fetchFromNovalaunchRank;
        renderFunction = renderForMagiceden;
      } else if (
        location.href.match("https://alpha.art/collection/lucky-kittens")
      ) {
        collection = `lucky-kittens`;
        itemKey = `tm-addrank-alphaart-${collection}`;
        fetchFunctionData = { itemKey };
        fetchFunction = fetchFromLuckyKittensRank;
        renderFunction = renderForAlphaart;
      } else if (
        location.href.match("https://alpha.art/collection/monkey-kingdom")
      ) {
        collection = `monkey-kingdom`;
        itemKey = `tm-addrank-alphaart-${collection}`;
        fetchFunctionData = { site: `monkeykingdom.io` };
        fetchFunction = fetchFromNovalaunchRank;
        renderFunction = renderForAlphaart;
      } else if (location.href.match("https://magiceden.io/marketplace/")) {
        collection = location.pathname.match(/^\/marketplace\/(.*?)$/)[1] || ``;
        itemKey = `tm-addrank-magiceden-${collection}`;
        fetchFunctionData = { collections: [collection], itemKey };
        fetchFunction = fetchFromMoonrank;
        renderFunction = renderForMagiceden;
      } else if (location.href.match("https://alpha.art/collection/")) {
        collection =
          location.pathname.match(/^\/collection\/(.*?)($|\/)/)[1] || ``; //items or activity page
        itemKey = `tm-addrank-alphaart-${collection}`;
        fetchFunctionData = {
          collections: [
            collection.replaceAll("-", "_"),
            collection.replaceAll("-", ""),
          ],
          itemKey,
        };
        fetchFunction = fetchFromMoonrank;
        renderFunction = renderForAlphaart;
      } else {
        clearCacheButton.innerText = ClearCacheButtonNA;
      }
      let item = JSON.parse(localStorage.getItem(itemKey) || `{}`);
      let rankList = [];
      if (
        !item ||
        !item.lastUpdated ||
        (!item.rankList.length &&
          new Date().getTime() > ITEM_EXPIRE + item.lastUpdated)
      ) {
        if (typeof fetchFunction === "function") {
          rankList = await fetchFunction(fetchFunctionData);
          if (rankList.error) {
            rankList = [];
          }
          addRankListToLocalStorage(itemKey, rankList);
          console.log("added to local storage");
        }
      } else {
        rankList = item.rankList;
      }

      renderFunction(rankList);
    }
  };

  setTimeout(addRank, 0); //force run once at pageload
  document.body.addEventListener("click", () => {
    addRank();
  });
})();
